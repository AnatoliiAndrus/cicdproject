FROM openjdk:11

RUN apt-get update
RUN apt-get install -y maven

WORKDIR /app

COPY . /app

RUN mvn clean package

EXPOSE 8080

CMD ["java", "-jar", "target/AutoCICD.jar"]