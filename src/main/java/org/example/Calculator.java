package org.example;

public class Calculator {
    private double value = 0;
    private boolean calculating = false;

    public Calculator base(double baseValue){
        this.value = baseValue;
        this.calculating = true;
        return this;
    }

    public Calculator add(double valueToAdd){
        if(calculating){
            value+=valueToAdd;
            return this;
        }
        return null;
    }

    public Calculator subtract(double valueToSubtract){
        if(calculating){
            value-=valueToSubtract;
            return this;
        }
        return null;
    }

    public Calculator multiply(double value){
        if(calculating){
            this.value*=value;
            return this;
        }
        return null;
    }

    public Calculator divide(double value){
        if(calculating){
            this.value/=value;
            return this;
        }
        return null;
    }

    public double calculate(){
        if(calculating){
            calculating = false;
            return value;
        }
        return 0;
    }

}
