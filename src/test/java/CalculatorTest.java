import org.example.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    @Test
    public void testCalculator(){
        double x = 10;
        double y = 5;
        double z = 8;
        Calculator calculator = new Calculator();

        Assertions.assertEquals(y+x-z, calculator.base(y).add(x).subtract(z).calculate());
    }

    @Test
    public void otherTestCalculator(){
        double x = 10;
        double y = 5;
        double z = 8;
        Calculator calculator = new Calculator();

        Assertions.assertEquals((y*x)-z, calculator.base(y).multiply(x).subtract(z).calculate());
    }
}
